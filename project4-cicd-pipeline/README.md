**&larr; [Back to Program README](../README.md)**
# Project 4 CI/CD Design

  * [Introduction](#introduction)
  * [Reference Solution](#reference-solution)
---
## Introduction

Project 4 is concerned with automating the building of the Estate application via  CI/CD pipeline.   For the purpose of this project, deployment will be to a local folder.


![](./docs/images/pipeline.png)
<figcaption><b>Fig.1 - CI/CD</b></figcaption>

## Reference Solution
```javascript   
pipeline {
    agent any
    stages {
        stage ('Build React') {
            steps {
                git url: 'https://github.com/agray998/simple-node-js-react-npm-app'
                
                bat "npm install"
                bat "npm test"
                bat "npm package"
            }
        }
        stage ('Build Maven') {
            steps {

                git url: 'https://github.com/agray998/SpringBoot-Jenkins',
                    branch: 'main'

                withMaven {

                // Run the maven build
                bat "mvn clean package" // deploy also runs all phases prior to deploy

                } // withMaven will discover the generated Maven artifacts, JUnit Surefire & FailSafe & FindBugs & SpotBugs reports...
            }
        }
    }
    post {
        always {
            mail to: you@example.com,
                 subject: "Jenkins build ${env.BUILD_NUMBER}",
                 body: "Build no. ${env.BUILD_NUMBER} finished with status: ${currentBuild.currentResult}."
        }
    }
}
```
